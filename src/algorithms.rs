//! Graph algorithms implemented on abstract graphs

pub mod bfs;
pub mod dfs;
pub mod dijkstra;
